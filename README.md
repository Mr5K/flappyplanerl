# Flappy Plane Agent
This is an simple implementation of flappy plane (like the well known mobile game flappy bird).
We used the [ml-agent framework](https://github.com/Unity-Technologies/ml-agents) to train an reinforcment learning agent to master this game.