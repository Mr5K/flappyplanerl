﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
//using Unity.MLAgents.Actuators;

public class playerMovement : Agent
{

    [SerializeField] float force;
    [SerializeField] managerBehav manager;
    Vector2 startPosition;
    private bool flapped;

    Rigidbody2D rb;
    float height = 2;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
    }


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpUp();
        }
    }

    public override void OnEpisodeBegin()
    {
        transform.position = startPosition;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(transform.position.y / height);
        sensor.AddObservation(gameObject.GetComponent<RayPerceptionSensorComponent2D>());
        sensor.AddObservation(flapped ? 1f : -1f);
        sensor.AddObservation(rb.velocity / height);
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        AddReward(Time.fixedDeltaTime);
        int flap = Mathf.FloorToInt(vectorAction[0]);
        if (flap == 1)
        {
            jumpUp();
            flapped = true;
        }
        else
        {
            flapped = false;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "trigger")
        {
            manager.pushScore();
            SetReward(1.0f);
        }
        else if (collision.gameObject.tag == "walls")
        {
            //SetReward(-1f);
            EndEpisode();
            Debug.Log("Score: " + manager.getScore());
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        SetReward(-1.0f);
        EndEpisode();
        Debug.Log("Score: " + manager.getScore());
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void jumpUp()
    {
        //AddReward(0.1f);
        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up * force);
    }
}
