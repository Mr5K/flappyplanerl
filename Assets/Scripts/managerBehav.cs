﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class managerBehav : MonoBehaviour {

    // Use this for initialization
    [SerializeField] Text scoreText;
    [SerializeField] GameObject obstc;
    
    int score = 0;

	void Start () {
        StartCoroutine(spawnObstacle());
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }

    public void pushScore()
    {
        score += 1;
        scoreText.text = "Score: " + score;
    }

    public int getScore(){
        return score;
    }


    IEnumerator spawnObstacle()
    {
        Instantiate(obstc, randomPosi(), Quaternion.identity);
        yield return new WaitForSeconds(0.8f);
        StartCoroutine(spawnObstacle());
    }

    Vector3 randomPosi()
    {
        Vector3 posi = new Vector3(9.54f, 0, -1);
        posi.y=Random.Range(-1.4f, 2.4f);
        return posi;
    }
}
