﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleMovement : MonoBehaviour {
    [SerializeField] float speed;

	void Update () {
        transform.Translate(Vector2.left * Time.deltaTime * speed);
        if (transform.root.position.x <= -10)
        {
            Destroy(gameObject);
        }
	}
}
